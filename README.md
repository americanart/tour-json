# tour.json
A [JSON schema](http://json-schema.org/) implementation defining the structure of JSON data for mobile tours of a physical 
space. The intent is replicate the goals of the [TourML specification](https://www.tourml.org/), which are to represent 
the contents and structure of mobiles tours, allowing for reuse across platforms, while taking advantage of the format 
and features of JSON Schema. 

## Tour
JSON schema for a Tour, contains data used to delivery a mobile tour of a physical space.
- [tour.schema.json](https://gitlab.com/americanart/tour-json/blob/master/tour.schema.json)

## Tour Set
JSON Schema for a collection of Tours
- [tour-set.schema.json](https://gitlab.com/americanart/tour-json/blob/master/tour-set.schema.json)

## Contributing
Create an [issue](https://gitlab.com/americanart/tour-json/issues) or submit Pull Requests to `draft`

## Links/Research
- [TourML](https://www.tourml.org/): about the TourML specification
- Download [TourML v1.0](https://github.com/IMAmuseum/tourml/blob/master/TourML.xsd)
- [JSON schema](http://json-schema.org/): getting started
- [JSON Schema Validator](https://www.jsonschemavalidator.net/): Online validator
- [JSON Schema for GeoJSON](https://github.com/geojson/schema)